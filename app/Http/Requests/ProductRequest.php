<?php

namespace App\Http\Requests;

use App\Enums\ProductStatusEnum;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
       return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'code' => [ 'required', 'integer'],
            'status' => [new Enum(ProductStatusEnum::class)],
            'url' => [ 'required', 'string'],
            'creator' => [ 'required', 'string'],
            'name' => [ 'required', 'string'],
            'brands' => [ 'required', 'string'],
            'categories' => [ 'required', 'string'],
            'quantity' => [ 'required', 'string'],
            'mainCategory' => [ 'required', 'string'],
            'labels' => [ 'required', 'string'],
            'traces' => [ 'required', 'string'],
            'servingQuantity' => [ 'required', 'numeric'],
            'nutriScore' => [ 'required', 'integer'],
            'nutriScoreGrade' => [ 'required', 'string'],
            'servingSize' => [ 'required', 'string'],
            'cities' => [ 'required', 'string'],
            'purchasePlaces' => [ 'required', 'string'],
            'stores' => [ 'required', 'string'],
            'imageUrl' => [ 'required', 'string'],
            'ingredientsText' => [ 'required', 'string'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\CheckApiService;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * Class CheckApiController
 * @package  App\Http\Controllers
 */

class CheckApiController extends Controller
{
    public function __construct(
        protected CheckApiService $checkApiService
    )
    {
    }

    /**
     * @OA\Post(
     *      tags={"/"},
     *      summary="Present a list of api log resources ",
     *      description="Get all log Api",
     *      path="/",
     *      security={{"bearerAuth": {}}},
     *      @OA\Response(
     *          response="200", description="List of log Api"
     *      )
     * )
     */
    public function index(): JsonResponse
    {
        $return = $this->checkApiService->getCheckApi()->toArray();
        return response()->json($return, ResponseAlias::HTTP_OK);
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\DataTransferObjects\ProductDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Interfaces\ProductServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * Class ProductControllerController
 * @package  App\Http\Controllers
 */
class ProductController extends Controller
{
    public function __construct(
        protected readonly ProductServiceInterface $productService
    )
    {
    }

    /**
     * @OA\Get(
     *      tags={"/produtos"},
     *      summary="Display a listing of the resource and paginate then",
     *      description="Get all products on database",
     *      path="/produtos",
     *      security={{"bearerAuth": {}}},
     *      @OA\Response(
     *          response="200", description="List of products"
     *      )
     * )
     */
    public function index(): JsonResponse
    {
        $return = $this->productService->listProducts();
        return response()->json($return, ResponseAlias::HTTP_OK);
    }

    /**
     * @OA\Get(
     *      tags={"/produtos"},
     *      summary="Display only one product",
     *      description="Get a product from the database",
     *      path="/produtos/{code}",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="ProductDto id",
     *          in="path",
     *          name="code",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200", description="Show a product"
     *      )
     * )
     */
    public function show(int $id): JsonResponse
    {
        $return = $this->productService->showProduct($id);
        return response()->json($return, ResponseAlias::HTTP_OK);
    }

    /**
     * @OA\Post(
     *      tags={"/produtos"},
     *      summary="Create a product",
     *      description="Create a product on database",
     *      path="/produtos",
     *      security={{"bearerAuth": {}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="string"),
     *              @OA\Property(property="url", type="string"),
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="quantity", type="string"),
     *              @OA\Property(property="brands", type="string"),
     *              @OA\Property(property="categories", type="string"),
     *              @OA\Property(property="labels", type="string"),
     *              @OA\Property(property="cities", type="string"),
     *              @OA\Property(property="purchasePlaces", type="string"),
     *              @OA\Property(property="stores", type="string"),
     *              @OA\Property(property="ingredientsText", type="string"),
     *              @OA\Property(property="traces", type="string"),
     *              @OA\Property(property="servingSize", type="string"),
     *              @OA\Property(property="servingQuantity", type="integer"),
     *              @OA\Property(property="nutriScore", type="integer"),
     *              @OA\Property(property="nutriScoreGrade", type="string"),
     *              @OA\Property(property="mainCategory", type="string"),
     *              @OA\Property(property="imageUrl", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response="201", description="create a product"
     *      ),
     *      @OA\Response(
     *          response="400", description="Bad request"
     *      )
     * )
     */
    public function store(ProductRequest $request): JsonResponse
    {
        try {
            $this->productService->createProduct(
                ProductDto::fromApiRequest($request));
            return response()->json([], ResponseAlias::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(
                $th->getMessage(),
                ResponseAlias::HTTP_BAD_REQUEST,
            );
        }
    }

    /**
     * @OA\Put(
     *      tags={"/produtos"},
     *      summary="Update a product",
     *      description="Update a product on database",
     *      path="/produtos/{code}",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="ProductDto id",
     *          in="path",
     *          name="code",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="status", type="string"),
     *              @OA\Property(property="url", type="string"),
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="quantity", type="string"),
     *              @OA\Property(property="brands", type="string"),
     *              @OA\Property(property="categories", type="string"),
     *              @OA\Property(property="labels", type="string"),
     *              @OA\Property(property="cities", type="string"),
     *              @OA\Property(property="purchasePlaces", type="string"),
     *              @OA\Property(property="stores", type="string"),
     *              @OA\Property(property="ingredientsText", type="string"),
     *              @OA\Property(property="traces", type="string"),
     *              @OA\Property(property="servingSize", type="string"),
     *              @OA\Property(property="servingQuantity", type="integer"),
     *              @OA\Property(property="nutriScore", type="integer"),
     *              @OA\Property(property="nutriScoreGrade", type="string"),
     *              @OA\Property(property="mainCategory", type="string"),
     *              @OA\Property(property="imageUrl", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response="202", description="Update a product"
     *      ),
     *      @OA\Response(
     *          response="400", description="Bad request"
     *      )
     * )
     */
    public function update(int $id, ProductRequest $request): JsonResponse
    {
        try {
            $return = $this->productService
                ->updateProduct($id, ProductDto::fromApiRequest($request));

            return response()->json($return, ResponseAlias::HTTP_ACCEPTED);
        } catch (\Throwable $th) {
            return response()->json(
                $th->getMessage(),
                ResponseAlias::HTTP_BAD_REQUEST,
            );
        }
    }

    /**
     * @OA\Delete(
     *      tags={"/produtos"},
     *      summary="Change the product status",
     *      description="Change the product status to 'trash'",
     *      path="/produtos/{code}",
     *      security={{"bearerAuth": {}}},
     *      @OA\Parameter(
     *          description="ProductDto id",
     *          in="path",
     *          name="code",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response="204", description="Change the product status"
     *      )
     * )
     */
    public function destroy(int $id): Response
    {
        $this->productService->disableProduct($id);

        return response()->noContent();
    }
}

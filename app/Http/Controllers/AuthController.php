<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Traits\HttpResponses;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;

/**
 * Class AuthController
 * @package  App\Http\Controllers
 */
class AuthController extends Controller
{
    use HttpResponses;


    /**
     *
     * @param Request $request
     *
     * * @OA\Post(
     *      tags={"/auth"},
     *      path="/login",
     *      summary="User Login",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="email", type="email"),
     *              @OA\Property(property="password", type="password"),
     *          )
     *      ),
     *      @OA\Response(
     *              response="201",
     *              description="",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(property="message", type="string"),
     *                  @OA\Property(property="status", type="integer", example=201),
     *                  @OA\Property(
     *                      property="data",
     *                      type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="token", type="string")
     *                       )
     *                  ),
     *              )
     *      )
     * )
     *
     * @return JsonResponse
     */

    public function login(Request $request): JsonResponse
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            return $this->response('Authorized', 201, [
                'token' => $request->user()
                    ->createToken($request->userAgent())
                    ->plainTextToken
            ]);
        }

        return $this->response('Not Authorized', 403);
    }

    /**
     *
     * @param Request $request
     *
     * * @OA\Post(
     *      tags={"/auth"},
     *      path="/logout",
     *      summary="User logout",
     *      @OA\Response(
     *              response="200",
     *              description="",
     *              @OA\JsonContent(
     *                  type="object",
     *                  @OA\Property(property="message", type="string", example="Token Revoked"),
     *                  @OA\Property(property="status", type="integer", example=200),
     *              )
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return $this->response('Token Revoked', 200);
    }
}

<?php

declare(strict_types=1);

namespace App\Logs;

use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class  LogCron
{
    protected LoggerInterface $logSchedule;

    public function __construct()
    {
        $this->logSchedule = Log::build([
            'driver' => 'single',
            'path' => storage_path('app/public/logs/schedule.log'),
        ]);
    }

    public function logScheduleInfo(string $message): void
    {
        $this->logSchedule->info($message);
    }

    public function logScheduleError(string $message): void
    {
        $this->logSchedule->error($message);
    }
}

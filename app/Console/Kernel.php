<?php

namespace App\Console;

use App\Console\Commands\SyncProductsCommand;
use App\Logs\LogCron;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(SyncProductsCommand::class)->everyTwoMinutes()
            ->before(function () {
                Cache::add('last_cron_time', date("Y-m-d H:i:s"));
                (new LogCron)->logScheduleInfo('Ultimo Horario do Cron : ' . date("Y-m-d H:i:s"));
            });
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

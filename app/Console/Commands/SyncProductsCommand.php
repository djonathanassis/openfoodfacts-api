<?php

namespace App\Console\Commands;

use App\Interfaces\ProductServiceInterface;
use App\Jobs\ProcessProductJob;
use App\Services\Partners\OpenFoodFacts\Facades\OpenFoodFacts;
use Illuminate\Console\Command;

class SyncProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add products from external api';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        ProcessProductJob::dispatch();
    }
}

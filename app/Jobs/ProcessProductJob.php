<?php

namespace App\Jobs;

use App\Services\HealthCheckService;
use App\Services\Partners\OpenFoodFacts\Facades\OpenFoodFacts;
use App\Services\ProductService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        $this->queue = 'products';
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        OpenFoodFacts::products()->syncProducts();
    }

}

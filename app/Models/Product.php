<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable =
    [
        'code',
        'status',
        'imported_t',
        'url',
        'creator',
        'created_t',
        'last_modified_t',
        'name',
        'quantity',
        'brands',
        'categories',
        'labels',
        'cities',
        'purchasePlaces',
        'stores',
        'ingredientsText',
        'traces',
        'servingSize',
        'servingQuantity',
        'nutriScore',
        'nutriScoreGrade',
        'mainCategory',
        'imageUrl',
    ];
}

<?php

declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface CheckApiServiceInterface
{
    public function getCheckApi(string $url): Collection;
}

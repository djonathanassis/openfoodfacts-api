<?php

namespace App\Interfaces;

use App\DataTransferObjects\ProductDto;
use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface ProductServiceInterface
{
    public function listProducts();

    public function createProduct(ProductDto $request): void;

    public function showProduct(int $code): Product;

    public function updateProduct(int $code, ProductDto $request): Product;

    public function disableProduct(int $code): void;

}

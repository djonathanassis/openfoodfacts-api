<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\CheckApi;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

trait CheckApiTrait
{
    private array $statusPerService = [];

    private array $systemMetaData = [];

    private int $httpStatusCode = 200;

    private string $statusMessage = 'ok';

    private string $memoryConsumed;

    private array $services = [
        'database',
    ];

    private array $systemChecks = [
        'memory',
    ];


    /**
     * @throws \JsonException
     */
    public function healthStatus(): void
    {
        $content = $this->gatherHealthFacts();

        CheckApi::create([
            'status' => $this->getStatusMessage(),
            'content' => $content,
            'lastTimeCron' => $this->getLastTimeCron(),
            'memoryConsumed' => $this->getMemory(),
        ]);

        Cache::clear();
    }


    protected function getDefaultDbConnectionName(): string
    {
        return config('database.default');
    }

    private function getLastTimeCron()
    {
        return Cache::get('last_cron_time');
    }

    private function setStatusCode(): void
    {
        $this->httpStatusCode = 503;
    }


    private function getStatusCode(): int
    {
        return $this->httpStatusCode;
    }


    private function setStatusMessage(string $statusMessage): void
    {
        $this->statusMessage = $statusMessage;
    }


    private function getStatusMessage(): string
    {
        return $this->statusMessage;
    }

    private function getMemory(): string
    {
        $memory = memory_get_usage();
        return round($memory / 1024) . 'KB';
    }


    private function checkDatabaseConnection(): void
    {

        $dbConnectionName = $this->getDefaultDbConnectionName();

        try {
            DB::connection($dbConnectionName)->getPdo();
            $this->setServiceStatus('up');
        } catch (\Exception $exception) {
            $this->setStatusCode();
            $this->setStatusMessage('error');
            $this->setServiceStatus('error', $exception->getMessage());
        }

    }

    protected function memoryCheck(): void
    {
        $limitPercentage = 90;
        $memoryUsed = (memory_get_usage(true) / 1024) / 1024;
        $memoryLimit = intval(ini_get('memory_limit'));
        $precentageUsed = number_format(($memoryUsed / $memoryLimit) * 100, 1);

        if ($precentageUsed >= $limitPercentage) {
            $this->setStatusMessage('warning');
            $this->setSystemMetaFact('warning', 'warning, the memory is ' . $precentageUsed . '% full');
        } else {
            $this->setSystemMetaFact('ok', $precentageUsed . '% used');
        }

    }

    /**
     * @throws \JsonException
     */
    private function gatherHealthFacts(): string
    {

        foreach ($this->services as $service) {
            $checkFunction = 'check' . ucfirst($service) . 'Connection';
            $this->$checkFunction();
        }

        foreach ($this->systemChecks as $system) {
            $checkFunction = ucfirst($system) . 'Check';
            $this->$checkFunction();
        }

        return json_encode($this->fullSystemHealthStatus(), JSON_THROW_ON_ERROR);
    }


    private function setServiceStatus(string $status, string $message = 'good'): void
    {
        $serviceItemStatus = [
            'Database' =>
                [
                    "status" => $status,
                    "message" => $message
                ]
        ];

        $this->statusPerService[] = $serviceItemStatus;

    }


    private function setSystemMetaFact(string $status, string $message = ''): void
    {
        $systemItemStatus = [
            'php_memory_used' =>
                [
                    "status" => $status,
                    "message" => $message
                ]
        ];

        $this->systemMetaData[] = $systemItemStatus;

    }

    private function fullSystemHealthStatus(): array
    {

        return [
            "status" => $this->getStatusMessage(),
            "status_code" => $this->getStatusCode(),
            "info" => $this->statusPerService,
            "system_info" => $this->systemMetaData
        ];

    }
}

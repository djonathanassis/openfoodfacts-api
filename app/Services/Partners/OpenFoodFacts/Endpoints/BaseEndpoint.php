<?php

declare(strict_types=1);

namespace App\Services\Partners\OpenFoodFacts\Endpoints;

use App\Services\Partners\OpenFoodFacts\OpenFoodFactsService;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class BaseEndpoint
{
    public function __construct(
        protected OpenFoodFactsService $service = new OpenFoodFactsService()
    )
    {}

    protected function getFiles(): Collection
    {
        $response = $this->service->api->get('/food/data/json/index.txt');
        return Str::of($response->body())->explode("\n")->reject(function ($value) {
            return empty($value);
        });
    }

    protected function transform(mixed $json, string $entity): Collection
    {
        return collect($json)
            ->map(fn($item) => new $entity($item));
    }
}

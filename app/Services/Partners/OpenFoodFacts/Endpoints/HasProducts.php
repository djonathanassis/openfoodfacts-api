<?php

namespace App\Services\Partners\OpenFoodFacts\Endpoints;

trait HasProducts
{
    public function products(): Products
    {
        return new Products();
    }

}

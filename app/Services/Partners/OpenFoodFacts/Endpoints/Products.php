<?php

declare(strict_types=1);

namespace App\Services\Partners\OpenFoodFacts\Endpoints;

use App\Logs\LogCron;
use App\Models\Product as ProductModel;
use App\Services\Partners\OpenFoodFacts\Dto\ProductDto;
use App\Traits\CheckApiTrait;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Products extends BaseEndpoint
{
    use CheckApiTrait;

    public const MAX_PRODUCTS_PER_FILE = 100;

    /**
     * @throws \JsonException
     */
    public function syncProducts(): void
    {
        try {
            $this->importedFiles()?->map(function ($product) {
                foreach ($product->toArray() as $item) {
                    $this->saveProduct($item);
                }
            });
        } catch (\Throwable $th) {
            (new LogCron)->logScheduleError($th->getMessage());
        } finally {
            $this->healthStatus();
            (new LogCron)->logScheduleInfo('Sincronização dos produtos concluída: ' . date("Y-m-d H:i:s"));
        }
    }

    protected function importedFiles(): ?Collection
    {
        try {
            return $this->getFiles()->map(function ($file) {
                $handle = gzdecode(file_get_contents("https://challenges.coode.sh/food/data/json/" . $file));

                $collection = Str::of($handle)
                    ->replace(['"\\', '\r', ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'], '')
                    ->explode("\n", self::MAX_PRODUCTS_PER_FILE);

                $collection->pop();

                return $this->transform($collection->map(
                    fn($item) => json_decode($item, true, 512, JSON_THROW_ON_ERROR))->all(),
                    ProductDto::class);
            });
        } catch (\Throwable $th) {
            (new LogCron)->logScheduleError($th->getMessage());
            return null;
        }
    }

    protected function saveProduct(ProductDto $productDto): void
    {
        ProductModel::updateOrCreate(
            ['code' => $productDto->code],
            $productDto->toArray()
        );
    }
}




<?php

declare(strict_types=1);

namespace App\Services\Partners\OpenFoodFacts;

use App\Services\Partners\OpenFoodFacts\Endpoints\HasProducts;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class OpenFoodFactsService
{
   use HasProducts;

    public PendingRequest $api;

   public function __construct()
   {
       $this->api = Http::baseUrl('https://challenges.coode.sh');
   }


}

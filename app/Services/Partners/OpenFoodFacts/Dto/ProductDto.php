<?php

declare(strict_types=1);

namespace App\Services\Partners\OpenFoodFacts\Dto;

use App\DataTransferObjects\AbstractDto;
use App\Enums\ProductStatusEnum;

class ProductDto extends AbstractDto
{
    readonly public int $code;

    readonly public ProductStatusEnum $status;

    readonly public ?string $url;

    readonly public ?string $creator;
    readonly public int $created_t;
    readonly public int $last_modified_t;

    readonly public ?string $name;
    readonly public ?string $quantity;
    readonly public ?string $brands;
    readonly public ?string $categories;
    readonly public ?string $labels;
    readonly public ?string $cities;
    readonly public ?string $purchasePlaces;

    readonly public ?string $stores;
    readonly public ?string $ingredientsText;
    readonly public ?string $traces;
    readonly public ?string $servingSize;
    readonly public float $servingQuantity;
    readonly public int $nutriScore;
    readonly public ?string $nutriScoreGrade;
    readonly public ?string $mainCategory;
    readonly public ?string $imageUrl;

    /**
     * @throws \Exception
     */
    public function __construct($data)
    {
        $this->code = (int) data_get($data, 'code');
        $this->status = ProductStatusEnum::PUBLISHED;
        $this->url = data_get($data, 'url');
        $this->creator = data_get($data, 'creator');
        $this->created_t = (int) data_get($data, 'created_t');
        $this->last_modified_t = (int) data_get($data, 'last_modified_t');
        $this->name = data_get($data, 'product_name');
        $this->quantity = data_get($data, 'quantity');
        $this->brands = data_get($data, 'brands');
        $this->categories = data_get($data, 'categories');
        $this->labels = data_get($data, 'labels');
        $this->cities = data_get($data, 'cities');
        $this->purchasePlaces = data_get($data, 'purchase_places');
        $this->stores = data_get($data, 'stores');
        $this->ingredientsText = data_get($data, 'ingredients_text');
        $this->traces = data_get($data, 'traces');
        $this->servingSize = data_get($data, 'serving_size');
        $this->servingQuantity = (float) data_get($data, 'serving_quantity');
        $this->nutriScore = (int) data_get($data, 'nutriscore_score');
        $this->nutriScoreGrade = data_get($data, 'nutriscore_grade');
        $this->mainCategory = data_get($data, 'main_category');
        $this->imageUrl = data_get($data, 'image_url');
    }
}

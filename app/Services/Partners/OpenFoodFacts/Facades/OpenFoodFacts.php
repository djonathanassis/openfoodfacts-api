<?php

declare(strict_types=1);

namespace App\Services\Partners\OpenFoodFacts\Facades;

use App\Services\Partners\OpenFoodFacts\OpenFoodFactsService;
use Illuminate\Support\Facades\Facade;

/**
 * @method static products()
 */
class OpenFoodFacts extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return OpenFoodFactsService::class;
    }
}

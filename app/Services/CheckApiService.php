<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\CheckApi;
use Illuminate\Database\Eloquent\Collection;

class CheckApiService extends BaseService
{
    public function __construct(CheckApi $checkApi)
    {
        parent::__construct($checkApi);
    }

    public function getCheckApi(): Collection
    {
        return $this->model->all();
    }
}


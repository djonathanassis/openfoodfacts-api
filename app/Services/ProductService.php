<?php

declare(strict_types=1);

namespace App\Services;

use App\DataTransferObjects\ProductDto;
use App\Enums\ProductStatusEnum;
use App\Interfaces\ProductServiceInterface;
use App\Models\Product;
use App\Services\Partners\OpenFoodFacts\Facades\OpenFoodFacts;
use Illuminate\Http\Request;

class ProductService extends BaseService implements ProductServiceInterface
{
    private const CODE = 'code';

    public function __construct(Product $product)
    {
        parent::__construct($product);
    }

    public function listProducts()
    {
        return $this->model->paginate();
    }

    public function showProduct(int $code): Product
    {
        return $this->model->where(self::CODE, $code)->first();
    }

    public function createProduct(ProductDto $request): void
    {
        $this->model->create($request->toArray());
    }

    public function updateProduct(int $code, ProductDto $request): Product
    {
        $product = $this->model->where(self::CODE, $code);

        if (!$id = $product->value('id')) {
            throw new \RuntimeException('ProductDto not found');
        }

        $product = $this->model->find($id);
        $product->update($request->toArray());
        return $product;

    }

    public function disableProduct(int $code): void
    {
        $this->model->where(self::CODE, $code)->update(['status' => ProductStatusEnum::TRASH]);
    }
}

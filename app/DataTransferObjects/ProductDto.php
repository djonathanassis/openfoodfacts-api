<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

use App\Enums\ProductStatusEnum;
use App\Http\Requests\ProductRequest;

class ProductDto extends AbstractDto
{
    public function __construct(
        readonly public int     $code,
        readonly public string  $status,
        readonly public ?string $url,
        readonly public ?string $creator,
        readonly public ?string $name,
        readonly public ?string $quantity,
        readonly public ?string $brands,
        readonly public ?string $categories,
        readonly public ?string $labels,
        readonly public ?string $cities,
        readonly public ?string $purchasePlaces,
        readonly public ?string $stores,
        readonly public ?string $ingredientsText,
        readonly public ?string $traces,
        readonly public ?string $servingSize,
        readonly public float   $servingQuantity,
        readonly public int     $nutriScore,
        readonly public ?string $nutriScoreGrade,
        readonly public ?string $mainCategory,
        readonly public ?string $imageUrl,
    )
    {
    }

    public static function fromApiRequest(ProductRequest $request): ProductDto
    {
        return new self(
            code: $request->validated('code'),
            status: $request->validated('status'),
            url: $request->validated('url'),
            creator: $request->validated('creator'),
            name: $request->validated('name'),
            quantity: $request->validated('quantity'),
            brands: $request->validated('brands'),
            categories: $request->validated('categories'),
            labels: $request->validated('labels'),
            cities: $request->validated('cities'),
            purchasePlaces: $request->validated('purchasePlaces'),
            stores: $request->validated('stores'),
            ingredientsText: $request->validated('ingredientsText'),
            traces: $request->validated('traces'),
            servingSize: $request->validated('servingSize'),
            servingQuantity: $request->validated('servingQuantity'),
            nutriScore: $request->validated('nutriScore'),
            nutriScoreGrade: $request->validated('nutriScoreGrade'),
            mainCategory: $request->validated('mainCategory'),
            imageUrl: $request->validated('imageUrl'),
        );
    }
}

<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\V1\CheckApiController;
use App\Http\Controllers\V1\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    Route::post('/auth/login', [AuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/', [CheckApiController::class, 'index']);
        Route::get('/products', [ProductController::class, 'index']);
        Route::get('/products/{code}', [ProductController::class, 'show']);
        Route::post('/products', [ProductController::class, 'store']);
        Route::put('/products/{code}', [ProductController::class, 'update']);
        Route::delete('/products/{code}', [ProductController::class, 'destroy']);
        Route::post('/auth/logout', [AuthController::class, 'logout']);
    });

});

<?php

namespace Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;


    public function testRequiresAuthentication(): void
    {
        $response = $this->postJson('/api/v1/auth/login');

        $response->assertStatus(422);
    }


    public function testAllowsAccessWithValidAuthentication(): void
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/v1/auth/login', [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(201);
    }
}

<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 *  Products API Tests
 */
class ProductControllerTest extends TestCase
{
    use RefreshDatabase;

    protected mixed $token;

    protected string $path = '/api/v1/products';

    public function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create();

        $response = $this->postJson('/api/v1/auth/login', [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $this->token = $response->json('token');
    }


    public function test_get_all_products_endpoint(): void
    {
        Product::factory(3)->create();
        $response = $this->getJson(
            uri:$this->path . '/',
            headers: [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ]);

        $response->assertStatus(200);

    }


    public function test_get_show_product_endpoint(): void
    {
        $product = Product::factory(1)->createOne();
        $response = $this->getJson(
            uri: $this->path . '/' . $product->code,
            headers: [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ]);

        $response->assertStatus(200);
    }


    public function test_store_product_endpoint(): void
    {
        $product = Product::factory()->make();

        $response = $this->postJson(
            uri: $this->path . '/',
            data: $product->toArray(),
            headers: [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ]
        );

        $response->assertStatus(201);
    }

    public function test_update_product_endpoint(): void
    {
        $product = Product::factory(1)->createOne();
        $newProduct = Product::factory()->make();


        $response = $this->putJson(
            uri: $this->path . '/' . $product->code,
            data: $newProduct->toArray(),
            headers: [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ]
        );

        $response->assertStatus(202);
    }


    public function test_delete_product_endpoint(): void
    {
        $product = Product::factory()->createOne();

        $response = $this->deleteJson(
            uri:$this->path . '/' . $product->code,
            headers: [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ]
        );

        $response->assertStatus(204);
    }
}

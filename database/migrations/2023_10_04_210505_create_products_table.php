<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('code');
            $table->enum('status', ['draft', 'trash', 'published']);
            $table->dateTime('imported_t')->default(now());
            $table->longText('url')->nullable();
            $table->string('creator')->nullable();
            $table->integer('created_t')->nullable();
            $table->integer('last_modified_t')->nullable();
            $table->string('name')->nullable();
            $table->string('quantity')->nullable();
            $table->string('brands')->nullable();
            $table->longText('categories')->nullable();
            $table->longText('labels')->nullable();
            $table->string('cities')->nullable();
            $table->string('purchasePlaces')->nullable();
            $table->string('stores')->nullable();
            $table->longText('ingredientsText')->nullable();
            $table->string('traces')->nullable();
            $table->string('servingSize')->nullable();
            $table->float('servingQuantity')->nullable();
            $table->integer('nutriScore')->nullable();
            $table->string('nutriScoreGrade')->nullable();
            $table->string('mainCategory')->nullable();
            $table->longText('imageUrl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('check_apis', static function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->longText('content')->nullable();
            $table->string('memoryConsumed');
            $table->dateTime('lastTimeCron')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('check_apis');
    }
};

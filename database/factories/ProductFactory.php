<?php

namespace Database\Factories;

use App\Enums\ProductStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code'             => $this->faker->randomNumber(),
            'status'           => ProductStatusEnum::PUBLISHED,
            'url'              => $this->faker->url,
            'creator'          => $this->faker->word(),
            'created_t'        => $this->faker->unixTime(),
            'last_modified_t'  => $this->faker->unixTime(),
            'name'             => $this->faker->word(),
            'quantity'         => $this->faker->word(),
            'brands'           => $this->faker->word(),
            'categories'       => $this->faker->sentence(),
            'labels'           => $this->faker->sentence(),
            'cities'           => $this->faker->word(),
            'purchasePlaces'   => $this->faker->word(),
            'stores'           => $this->faker->word(),
            'ingredientsText'  => $this->faker->sentence(),
            'traces'           => $this->faker->sentence(),
            'servingSize'      => $this->faker->sentence(),
            'servingQuantity'  => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
            'nutriScore'       => $this->faker->randomNumber(),
            'nutriScoreGrade'  => $this->faker->randomLetter(),
            'mainCategory'     => $this->faker->word(),
            'imageUrl'         => $this->faker->url(),
        ];
    }
}

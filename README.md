# Backend Challenge 20230105

## Descrição

Nesse desafio trabalharemos no desenvolvimento de uma REST API para utilizar os dados do projeto Open Food Facts, que é um banco de dados aberto com informação nutricional de diversos produtos alimentícios.
O projeto tem como objetivo dar suporte a equipe de nutricionistas da empresa Fitness Foods LC para que eles possam revisar de maneira rápida a informação nutricional dos alimentos que os usuários publicam pela aplicação móvel.

### Requisitos Obrigatórios

- Criar um banco de dados MongoDB usando Atlas: https://www.mongodb.com/cloud/atlas ou algum Banco de Dados SQL se não sentir confortável com NoSQL;
- Criar uma REST API com as melhores práticas de desenvolvimento, Design Patterns, SOLID e DDD.
- Integrar a API com o banco de dados criado para persistir os dados
- Recomendável usar Drivers oficiais para integração com o DB
- Desenvolver Testes Unitários


### Requisitos Extra

- Configuração de um endpoint de busca com Elastic Search ou similares;
- Configurar Docker no Projeto para facilitar o Deploy da equipe de DevOps;
- Configurar um sistema de alerta se tem algum falho durante o Sync dos produtos;
- Descrever a documentação da API utilizando o conceito de Open API 3.0;
- Escrever Unit Tests para os endpoints  GET e PUT do CRUD;
- Escrever um esquema de segurança utilizando `API KEY` nos endpoints. Ref: https://learning.postman.com/docs/sending-requests/authorization/#api-key


## Pré-Requisitos

* PHP 8.1;
* Laravel 10;
* Composer;
* Eloquent;
* Docker;
* PHPUnit;
* MySQL;
* RestAPI;

## Link para apresentação
https://www.loom.com/share/1e837b7202c0414ab06c0e7383b5f558?sid=e9c115b5-9ce0-4cd4-a321-7017a054d93f
## Instalação

Siga o passo a passo sobre como instalar e configurar do projeto:


#### 1. Clone o repositório:
```sh
git clone https://gitlab.com/djonathanassis/openfoodfacts-api.git
```

#### 2. Navegue até a pasta do projeto:
```sh
cd OpenFoodFacts-api
```

#### 3. Abra o VSCode ou IDE de sua preferência:
```sh
code .
```
#### 4. Renomeie o arquivo `.env.example` para `.env` e introduza suas variáveis de ambiente;

#### 5. Crie um alias para uso do docker:
```sh
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```

#### 6. Iniciar os Contêineres Docker:
```sh
sail up -d
```

#### 7. Instale as dependências usando o Composer (ou outro gerenciador de pacotes):
```sh
sail exec laravel.test composer install
```

#### 8. Crie uma chave de API com o Docker para o arquivo `.env`
```sh
sail artisan key:generate
```

Acesse a aplicação em seu navegador: **[http://localhost](http://localhost:8888)**

## Teste Unitario

Siga os passo a passo para rodar teste unitários:

#### 3. Comando para rodar os testes unitários:

```sh
 sail artisan test 
```
